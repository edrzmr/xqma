package dev.drrzmr.xqma.kamig.actor

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.ActorRef
import akka.actor.typed.ActorSystem
import akka.actor.typed.Behavior
import akka.cluster.sharding.typed.scaladsl.ClusterSharding
import akka.cluster.sharding.typed.scaladsl.Entity
import akka.cluster.sharding.typed.scaladsl.EntityTypeKey
import dev.drrzmr.xqma.kamig.domain
import dev.drrzmr.xqma.kamig.extension.cbor
import dev.drrzmr.xqma.kamig.extension.json

import scala.util.Failure
import scala.util.Success
import scala.util.Try

object Subject extends json.Helpers with json.DomainDecoders {

  val TypeKey: EntityTypeKey[Command] =
    EntityTypeKey[Command]("Subject")

  private val entity = Entity(TypeKey)(entityContext => Subject(entityContext.entityId))

  def initSharding(system: ActorSystem[_]): Unit = ClusterSharding(system).init(entity)

  def apply(entityId: String): Behavior[Command] = Behaviors.setup { ctx =>
    ctx.log.info(s"Starting subject $entityId")
    running(entityId, List.empty)
  }

  private def decodeValue(key: domain.SchemaRegistryKeySubject, raw: String): Try[domain.SchemaRegistryInfo] =
    key match {
      case k: domain.SchemaRegistryKafkaKeySchema =>
        if (raw == "null") Success(domain.SchemaRegistryInfo(k, domain.SchemaRegistryValueNull))
        else tryDecode[domain.SchemaRegistryValueSchema](raw).map(v => domain.SchemaRegistryInfo(k, v))
      case k: domain.SchemaRegistryKafkaKeyConfig =>
        if (raw == "null") Success(domain.SchemaRegistryInfo(k, domain.SchemaRegistryValueNull))
        else tryDecode[domain.SchemaRegistryValueConfig](raw).map(v => domain.SchemaRegistryInfo(k, v))
      case k: domain.SchemaRegistryKafkaKeyDeleteSubject =>
        tryDecode[domain.SchemaRegistryValueDeleteSubject](raw).map(v => domain.SchemaRegistryInfo(k, v))
    }

  private def running(entityId: String, infoList: List[domain.SchemaRegistryInfo]): Behavior[Command] =
    Behaviors.receive {
      case (_, SchemaCommand(key, value, replyTo: ActorRef[LineAdded])) =>
        decodeValue(key, value) match {
          case Failure(exception) =>
            replyTo ! ValueFailed(exception)
            Behaviors.same
          case Success(info: domain.SchemaRegistryInfo) =>
            replyTo ! LineAdded(entityId)
            running(entityId, info :: infoList)
        }

      case (ctx, Query(replyTo)) =>
        replyTo ! QueryResult(infoList.reverse)
        ctx.log.info(s"Subject $entityId receive a query from ${replyTo.toString}")
        Behaviors.same
      case (ctx, msg) =>
        ctx.log.info(s"Subject $entityId receive message $msg")
        Behaviors.same
    }

  /* Commands */
  sealed trait Command
  final case class SchemaCommand(key: domain.SchemaRegistryKeySubject, value: String, replyTo: ActorRef[LineEvent])
      extends Command
  final case class Query(replyTo: ActorRef[QueryResult]) extends Command

  /* Responses */
  final case class QueryResult(infoList: List[domain.SchemaRegistryInfo])

  /* Events */
  sealed trait LineEvent
  final case class LineAdded(entityId: String)   extends LineEvent with cbor.Serializable
  final case class ValueFailed(error: Throwable) extends LineEvent with cbor.Serializable
}
