package dev.drrzmr.xqma.kamig.http

import akka.actor.CoordinatedShutdown
import akka.actor.typed.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import akka.Done
import akka.{actor => classic}

import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.Failure
import scala.util.Success

object Server {
  val host = "localhost"
  val port = 8069

  val terminateDeadline: FiniteDuration = 10 seconds

  def apply(routes: Route, system: ActorSystem[_]): Server = new Server(routes, system)
}

class Server(routes: Route, system: ActorSystem[_]) {

  import Server._

  def run(): Unit = {

    import akka.actor.typed.scaladsl.adapter._
    implicit val classicSystem: classic.ActorSystem = system.toClassic
    val shutdown: CoordinatedShutdown               = CoordinatedShutdown(classicSystem)
    import system.executionContext

    Http().bindAndHandle(routes, host, port).onComplete {
      case Success(binding) =>
        val bindingHost = binding.localAddress.getHostString
        val bindingPort = binding.localAddress.getPort
        system.log.info(s"Listen at http://$bindingHost:$bindingPort")

        shutdown.addTask(CoordinatedShutdown.PhaseServiceRequestsDone, "http-graceful-terminate") { () =>
          binding
            .terminate(terminateDeadline)
            .map { _ =>
              system.log.info(s"Server http://$bindingHost:$bindingPort shutdown!")
              Done
            }
        }
      case Failure(exception) =>
        system.log.error(s"Failed to bind at $host:$port", exception)
    }
  }
}
