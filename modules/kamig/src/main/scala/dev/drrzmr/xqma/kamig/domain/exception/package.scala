package dev.drrzmr.xqma.kamig.domain

package object exception {
  final case class InvalidConfigException(message: String) extends Exception(message)
}
