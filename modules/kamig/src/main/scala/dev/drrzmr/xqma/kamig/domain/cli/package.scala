package dev.drrzmr.xqma.kamig.domain

package object cli {

  sealed trait Command
  case class DumpCommand(fn: String = null) extends Command

  sealed trait CommandName { val name: String }
  case object Dump  extends CommandName { val name = "dump" }
  case object Empty extends CommandName { val name = "node" }

  val validMode: Map[String, CommandName] = Map(
    Dump.name -> Dump
  )

  case class CommandLineParseException(args: Seq[String], message: String) extends Exception(message)
}
