package dev.drrzmr.xqma.kamig.actor

import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors
import dev.drrzmr.xqma.kamig.domain.cli
import dev.drrzmr.xqma.kamig.domain.config
import dev.drrzmr.xqma.kamig.stream

object Guardian {

  def apply(c: config.Config, command: cli.Command): Behavior[Nothing] =
    Behaviors.setup[Nothing] { ctx =>
      ctx.log.info("Starting the Guardian!")

      command match {
        case cli.DumpCommand(fn) =>
          stream.LoadFile(fn)(ctx.system).run()
      }

      Subject.initSharding(ctx.system)

      import dev.drrzmr.xqma.kamig.http
      val routes = http.Routes(ctx.system).mainRoute
      http.Server(routes, ctx.system).run()

      Behaviors.empty
    }
}
