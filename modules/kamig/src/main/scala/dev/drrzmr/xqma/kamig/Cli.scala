package dev.drrzmr.xqma.kamig

import java.io.File

import scopt.OParser
import dev.drrzmr.xqma.kamig.domain.cli._

import scala.util.Failure
import scala.util.Success
import scala.util.Try

case class Cli(args: Seq[String]) {
  import dev.drrzmr.xqma.kamig.service.SettingsService.settings

  val parser: OParser[Unit, Command] = {

    val builder = OParser.builder[Command]
    import builder._

    OParser.sequence(
      programName(settings.programName),
      head(settings.programName, settings.version),
      help('h', "help"),
      cmd(domain.cli.Dump.name)
        .text("handle dump file")
        .children(
          opt[File]("file")
            .abbr("f")
            .text("dump filename")
            .action((file, _) => DumpCommand(file.getAbsolutePath))
            .validate { file =>
              if (!file.exists()) failure(s"file ${file.getAbsolutePath} not found")
              else if (!file.canRead) failure(s"file ${file.getAbsolutePath} are not readable")
              else success
            }
        )
    )
  }

  def command: Try[Command] =
    OParser.parse(parser, args, null) match {
      case Some(command) => Success(command)
      case None =>
        Failure(CommandLineParseException(args, "something wrong with command line parser"))
    }
}
