package dev.drrzmr.xqma.kamig

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.util.Failure
import scala.util.Success
import akka.actor.typed.ActorSystem
import dev.drrzmr.xqma.kamig.domain.cli.Command
import dev.drrzmr.xqma.kamig.domain.cli.CommandLineParseException
import dev.drrzmr.xqma.kamig.domain.cli.DumpCommand

object Main extends App with service.ConfigurationService with service.SettingsService {

  println("kamig - The Ultimate Kafka Migration Toolkit")

  val command: Command = Cli(args).command match {
    case Success(command @ DumpCommand(_)) => command

    case Success(command)   => throw CommandLineParseException(args, s"command handler not implemented yet: $command")
    case Failure(exception) => throw exception
  }

  implicit val system: ActorSystem[Nothing] =
    ActorSystem[Nothing](actor.Guardian(configuration, command), settings.programName)

  system.whenTerminated.foreach(_ => println(">>> Actor System terminated"))(system.executionContext)
  Await.result(system.whenTerminated, Duration.Inf)
}
