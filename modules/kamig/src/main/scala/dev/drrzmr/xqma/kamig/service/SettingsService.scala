package dev.drrzmr.xqma.kamig.service

import dev.drrzmr.xqma.BuildInfo
import dev.drrzmr.xqma.kamig.domain

trait SettingsService {
  lazy val settings: domain.Settings = SettingsService.settings
}

object SettingsService {

  val settings: domain.Settings = domain.Settings(
    programName = "kamig",
    version = BuildInfo.version
  )
}
