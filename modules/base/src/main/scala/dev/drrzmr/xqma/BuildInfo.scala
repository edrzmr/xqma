package dev.drrzmr.xqma

case object BuildInfo {
  import build.Info._
  val version: String      = version
  val scalaVersion: String = scalaVersion
  val sbtVersion: String   = sbtVersion
  val timestamp: String    = builtAtString
}
