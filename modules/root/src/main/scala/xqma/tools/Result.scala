package xqma.tools

case class Result(message: String, exitCode: Int)

trait ResultType

object Result {
  case class Error(message: String, code: Int) extends ResultType
  case class Ok(message: String = "ok")        extends ResultType
  case object SilentOk                         extends ResultType
}
