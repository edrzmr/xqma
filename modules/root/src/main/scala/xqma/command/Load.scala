package xqma.command

import xqma.command.schemaregistry.{Subcommand => schemaRegistryCmd}
import xqma.tools.Logging
import xqma.tools.Settings

class Load(args: Seq[String]) extends Logging {

  Logging.setLevel(Settings.default.logLevel)

  private val parser = new scopt.OptionParser[Parameters](Settings.name) {

    head(Settings.name, Settings.version)

    help("help").abbr("h")

    opt[String]("config")
      .abbr("c")
      .optional()
      .action((fn, parameters) => parameters.copy(configFileName = fn))

    cmd("schema-registry")
      .action((_, parameters) => parameters.copy(command = Command.SchemaRegistry))
      .children(
        cmd("register")
          .action((_, parameters) => parameters.copy(subcommand = schemaRegistryCmd.Register))
          .required
          .children(
            opt[String]('t', "topic").required
              .action((topic, parameters) => parameters.copy(data = parameters.data.updated("topic", topic))),
            opt[String]('k', "key").optional
              .action((key, parameters) => parameters.copy(data = parameters.data.updated("key", key))),
            opt[String]('v', "value").optional
              .action((value, parameters) => parameters.copy(data = parameters.data.updated("value", value)))
          ),
      )
      .children(cmd("list-subjects")
        .action((_, parameters) => parameters.copy(subcommand = schemaRegistryCmd.ListSubjects)))
  }

  val parameters: Parameters = {
    val params = parser.parse(args, Parameters(commandLine = args)) match {
      case Some(p) => p
      case None =>
        logger.error("could not parse command line")
        System.exit(0)
        Parameters()
    }

    params.command match {
      case Command.None =>
        logger.warn(">>> unknown command")
        parser.showTryHelp()
        System.exit(0)
      case _ =>
    }

    params.subcommand match {
      case Subcommand.None =>
        logger.warn(">>> unknown subcommand")
        parser.showTryHelp()
        System.exit(0)
      case _ =>
    }

    params
  }

  val settings: Settings = {
    if (parameters.configFileName.isEmpty) {
      logger.info("loading default settings")
      Settings.default
    } else {
      logger.info(s"loading: ${parameters.configFileName} settings")
      Settings(parameters.configFileName)
    }
  }

  Logging.setLevel(settings.logLevel)
  logger.info(s"logLevel: ${settings.logLevel}")
  logger.debug(s"settings: ${settings.toString}")
}

object Load {

  def apply(args: Seq[String]): Load = new Load(args)
}
